# Web

**Web**, is the base role to setup apache2 with htdocs and create all needed users, groups and credentials.
Also some default files and templates will copy.

## Requirements

- apache2 **www-data** user **password**
  > the password is create and a hash is added into ./tasks/apache2.yml
  > **DEBUG:** only for current tests, the **password for www-data** => **swordfish**
  > nice to read and get info from here: <https://serversforhackers.com/c/create-user-in-ansible>
  - `$sudo apt install whois`
  - `$mkpasswd --method=sha-512`

## Role Variables

_A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well._

## Dependencies

_A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles._

## Example Playbook

Example how to use:

    - hosts: webserver
      roles:
         - web
           apache2_ssl_key_file: ansible.key
           apache2_ssl_crt_file: ansible.crt
           apache2_ssl_usage: vault # openssl

> When using `apache2_ssl_usage: openssl`, the vars **apache2_ssl_key_file** and **apache2_ssl_crt_file** are not needed.

## License

GNU AFFERO GENERAL PUBLIC LICENSE
