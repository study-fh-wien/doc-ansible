# Ansible <img height="25" width="25" src="https://cdn.jsdelivr.net/npm/simple-icons@v2/icons/ansible.svg" />

![version](https://img.shields.io/badge/version-0.0.1-blue)
![OS-Ubuntu](https://img.shields.io/badge/OS-ubuntu-E95420?logo=ubuntu)
![OS-proxmox](https://img.shields.io/badge/VIRT-proxmox-E57000?logo=proxmox)
![OS-ansible](https://img.shields.io/badge/Made%20with-ansible-EE0000?logo=ansible)

![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)
![made-with-VSCode](https://img.shields.io/badge/Made%20with-VSCode-1f425f.svg)

![GNU_AGPLv3 license](https://img.shields.io/badge/License-GNU_AGPLv3-blue.svg)
![Awesome Badges](https://img.shields.io/badge/badges-awesome-1dbbd5.svg)

```shell
  Authors:
    - Vladislav Masepohl
    - Andreas Schweiger
```

## Base short Ansible informations

```shell
    Local Maschine
      (Ansible)
      /   |   \
     /SSH | SSH\
    /     |     \
  Node  Node  Node
```

> - Ansible is a **push to node service**.
> - On a **local maschine** is ansible installed and has access to connect, for example over ssh, to the **nodes**.
> - Ansible will start, from the current **local maschine**, pushing the instructions, from the defined **playbook** and **inventory** files, to the **nodes**.

---

## Setup Ansible Env.

> Installations and setups are done under a virtual **Ubuntu Server 18.04** env. on **Proxmox** <img height="15" width="15" src="https://cdn.jsdelivr.net/npm/simple-icons@v2/icons/proxmox.svg" />

### Setup local client/host with Ansible and LXC containers

> **LXC** is setup like in the **last exercises**, so here will be no information how it was done.</br>
> Also it was done in LXC, create the **ssh-key** and let it add automatically with a **LXC profile**.

#### Install **Ansible** <img height="15" width="15" src="https://cdn.jsdelivr.net/npm/simple-icons@v2/icons/ansible.svg" />

```shell
$sudo apt install -y python3
$sudo apt install -y ansible
```

#### Setup **LXC** container

- Create **3 containers** for **_web1_**, **_web2_** and **_mail1_** + copied the ssh auth. keys into authorized_keys
  > **_ssh-keys_** are added per **_lxc profile automatically_**
  - `$lxc init --profile basic-all ubuntu:18.04/amd64 web1`
  - `$lxc init --profile basic-all ubuntu:18.04/amd64 web2`
  - `$lxc init --profile basic-all ubuntu:18.04/amd64 mail1`
  - `$lxc start web1 web2 mail1`
    - ```shell
      +-------+---------+-------------------+------+------------+-----------+
      | NAME  |  STATE  |       IPV4        | IPV6 |    TYPE    | SNAPSHOTS |
      +-------+---------+-------------------+------+------------+-----------+
      | mail1 | RUNNING | 10.0.1.190 (eth0) |      | PERSISTENT | 0         |
      +-------+---------+-------------------+------+------------+-----------+
      | web1  | RUNNING | 10.0.1.192 (eth0) |      | PERSISTENT | 0         |
      +-------+---------+-------------------+------+------------+-----------+
      | web2  | RUNNING | 10.0.1.191 (eth0) |      | PERSISTENT | 0         |
      +-------+---------+-------------------+------+------------+-----------+
      ```

#### Setup **Ansible** <img height="15" width="15" src="https://cdn.jsdelivr.net/npm/simple-icons@v2/icons/ansible.svg" />

##### Inventory and Ad-Hoc

- Create base folder structure
  - `$mkdir ~/ansible && cd ~/ansible`
  - `$mkdir inventory playbook`
- Create **`ansible.cfg`**
  - `$touch ansible.cfg`
  - with following content:
    - ```cfg
      # small copy from /etc/ansible/ansible.cfg, see there for more options
      [defaults]
      inventory         = ./inventory/inventory.yml
      roles_path        = ./roles
      host_key_checking = False
      [privilege_escalation]
      [paramiko_connection]
      [ssh_connection]
      scp_if_ssh = True
      [persistent_connection]
      [accelerate]
      [selinux]
      [colors]
      [diff]
      ```
- Create **inventory** and have **ping** try
  - `$touch inventory/inventory.yml`
  - with following content:
    - ```yml
      all:
        hosts:
          web1:
            ansible_user: ubuntu
            ansible_host: 10.0.1.192
            ansible_port: 22
          web2:
            ansible_user: ubuntu
            ansible_host: 10.0.1.191
            ansible_port: 22
          mail1:
            ansible_user: ubuntu
            ansible_host: 10.0.1.190
            ansible_port: 22
        vars:
          ansible_python_interpreter: /usr/bin/python3
        children:
          webserver:
            hosts:
              web1:
              web2:
          mailserver:
            hosts:
              mail1:
      ```
  - Check if everything is setup correct
    - With an ping test
      - `$ansible all -m ping`
        > You will get an **failed** output, that **python** is **not installed**,</br>
        > let's do that in the next step, if that is happen, else skip the install part
  - Install **python** over **Ad-Hoc** command
    > We also install directly **`python-apt`** for later playbook usage.</br>
    > Because python is not installed yet, we need to use **`-m raw`**
    - `$ansible all -m raw -a "apt -y install python3 python3-apt" -b`
      - **`-m raw`** => executes low-level commands, when python is missing on nodes
      - **`-b`** => run operation with become, here with sudo (on this ubuntu setup, the ubuntu user did not need enter a password)
      - **`install`** => we will install **python3** for ansible usage and **python3-apt** for the ansible apt task command
  - Check if everything is setup correct
    - With an ping test
      - `$ansible all -m ping`
      - Output should looks like that:
        - ```json
          web2 | SUCCESS => {
              "changed": false,
              "ping": "pong"
          }
          mail1 | SUCCESS => {
              "changed": false,
              "ping": "pong"
          }
          web1 | SUCCESS => {
              "changed": false,
              "ping": "pong"
          }
          ```
    - Or with run live command
      - `$ansible all -a "/bin/echo hello"`
      - output should looks like that:
        - ```shell
          web1 | SUCCESS | rc=0 >>
          hello
          #
          mail1 | SUCCESS | rc=0 >>
          hello
          #
          web2 | SUCCESS | rc=0 >>
          hello
          ```
  - Print from all nodes the file `/etc/hostname`
    - `$ansible all -a "cat /etc/hostname"`
    - Result should looks like that
      - ```shell
        web1 | SUCCESS | rc=0 >>
        web1
        #
        web2 | SUCCESS | rc=0 >>
        web2
        #
        mail1 | SUCCESS | rc=0 >>
        mail1
        ```

##### Playbook

- Create Playbook
  - `$touch playbooks/playbook.yml`
  - with following content:
    - ```yml
      ---
      - name: Base
        #become: true
        remote_user: ubuntu
        hosts:
          - webserver
          - mailserver
        roles:
          - security
          - updater
          - installer
          - ssh
          - security
      #
      - name: WebServer Playbook
        #become: true
        remote_user: ubuntu
        hosts:
          - webserver
        roles:
          - web
      #
      - name: MailServer PlayBook
        #become: true
        remote_user: ubuntu
        hosts:
          - mailserver
        roles:
          - smtp
      ```
- Create the roles
  - `$ansible-galaxy init security updater installer web smtp ssh`
    - **security** ✓
      - harding system like defined in _CIS Ubuntu Linux Benchmark_ for:
        - 2.2.4 Ensure CUPS is not enabled ✓
        - 6.1.2 Ensure permissions on /etc/passwd are configured ✓
          - \+ 6.1.\* Ensure permissions on \* are configured ✓
        - 1.3.3 Ensure sudo log file exists ✓
        - 2.3.4 Ensure telnet client is not installed ✓
    - **updater** ✓
      - updates all packages
    - **installer** ✓
      - installs some default packages (git, vim, openssl, ...)
    - **web** ✓
      - install and setup a webserver
      - setup a htdocs
    - **smtp**
      - install a SMTP daemon
    - **ssh** ✓
      - configure SSH-server like the recommendations from Mozilla
- Save credentials/files in **Ansible Vault**
  - For the **web** role we will copy a **config.php** into htdocs, where the mysql credentials are stored. This file should be stored encrypted in **ansible-vault**.
    - `$ansible-vault create roles/web/files/config.php`
      > you will ask to enter a new password and to confirm it.</br>
      > After that we can start enter our content, which we want to save in the encrypted vault file.
      >
      > **DEBUG:** only for current test, is our password: **_swordfish_**
      >
      > **DEBUG:** only for current test, is that the content we have entered:
      >
      > > ```conf
      > > [mysqldump]
      > > user=ansible
      > > password=swordfish
      > > ```
    - result:
      - ```yml
        $ANSIBLE_VAULT;1.1;AES256
        35303239306435313734666465353336376165356661333739663131356432383561363963393630
        6138653764666435643166663635643161316361336662640a336262313131353339666165386235
        32383135633365386366643739636462336537303135393839303736653136336631626138353163
        3132306663303434630a393537623130326232326663393265663562383465396234363361386664
        32343431366635343966306333646565356539393932316537363233303933363238643262383839
        3062353338366561633435333865656531633536343734623263
        ```
  - To use this encrypted file later with **`ansible-playbook`**, we need to provide the option **`--ask-vault-pass`**
    > You can also store it in the **ansible.cfg** file, like it is done here.</br>
    > Add for that, this line into **ansible.cfg**: **`ask_vault_pass = True`**
- Now we can start our playbook with our inventory
  - First perform a check, if everything is specified correct, with: **`--check`**
    - `$ansible-playbook playbooks/playbook.yml --check`
      > The check will not find all failures, so it can happen you will get failures on runtime and fix them also
  - When everything is correct, run the playbook
    - `$ansible-playbook playbooks/playbook.yml`

## Bonus

- Current the **web** role creates the files for https automatically. For the **bonus** exercise we will provide the private key and certificate over the ansible-vault
  - First we need a private key and a certificate, which we can create that way:
    - `$openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ansible.key -out ansible.crt`
  - The result files from openssl will now saved as ansible-vault files, to be then used by the **web** role
    - `$ansible-vault encrypt ansible.key --output ~/ansible/roles/web/files/ansible.key`
    - `$ansible-vault encrypt ansible.crt --output ~/ansible/roles/web/files/ansible.crt`
      > **DEBUG:** only for current test, is our password: **_swordfish_**

---

## Helpful

- ansible
  - <https://www.techrepublic.com/article/how-to-install-ansible-on-ubuntu-server-18-04/>
  - <https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html>
  - <https://docs.ansible.com/ansible/latest/modules/apt_module.html>
  - <https://docs.ansible.com/ansible/latest/modules/template_module.html>
- <https://infosec.mozilla.org/guidelines/openssh>
- <https://ssl-config.mozilla.org/#server=apache&version=2.4.41&config=modern&openssl=1.1.1d&guideline=5.4>
- <https://www.linode.com/docs/email/postfix/postfix-smtp-debian7/>
